Welcome to PepDocs!
=====================================
**PeppermintOS** is a Linux distro based on Debian/DeVuan 
This documentation can help you get better acquainted with the operating system 
online support can be found `here <https://forum.peppermintos.com/>`_



Contribute to the project
===========================
There are many ways you can contribute to Peppermint, you do not need to necessariy be a developer. 
 
- You can support other Pep's on the forum
- You can contribute to documentation 
- You can submit your wallpapers
- You can always provide feedback
- You can submit your ideas

*With the Peppermint Community, there are so many innovations we can build together.*

How to Contribute to PepDocs
============================
This documentation uses **sphinx**.
Sphinx is a tool that makes it easy to create intelligent and beautiful documentation, written by Georg Brandl and licensed under the BSD license.

All you need to get started is install sphinx to your computer you can learn how to do that using this tutorial `here <https://sphinx-tutorial.readthedocs.io/start/>`_  this turtorial also teaches you how to build your updates via sphinx.

After you get that part up and running you can then contact the Peppermint team via the forum to start get started on committing to the PepDocs repository out on codeberg.org.

**sphinx** uses reStructuredText (reST) you can read more about that `here <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_





This is Peppermint OS
======================
**Current Goals**

- bring longevity into the Peppermint OS build process
- modularize the Peppermint OS build process.
- build a continuous integration / delivery methodology for the Peppermint OS dev team
- build a thorough documentation wiki for Peppermint OS development processes
- introduce DevOps to the development team
- create pipelines for CI /CD
- enable development innovation not waterfall development
- move away from snapshot deployment
- build only on Debian stable repos
- don’t maintain our own repo eco-system(work with Debian not recreate it)
- minimum base but just enough to be easy for all to learn
- maximize choice where we can
- build non systemd (based on Devuan)
- use both Calamares and Debian installer
- deprecate all LXDE parts
- rebuild ICE
- rebuild update manager


CI / CD
========
This is the “pipeline” that we use for the Development team.

CI or Continuous Integration is the practice of automating the integration of code changes from multiple developers into a single code-base. It is a software development practice where the developers commit their work frequently into the central code repository (Codeberg). Then there are automated tools that build the newly committed code and do a code review, etc as required upon integration.

We already built the part in the process, to compile and build the code base. But we are still building the process for code review.

**Why CI?**

There have been scenarios when Peppermint developers, worked in isolation for an extended period of time and only merge their changes to the main branch once their work was completed. This not only makes the merging of code very difficult, prone to conflicts, and time-consuming but also results in bugs accumulating for a long time which are only identified in later stages of development. These factors make it harder to deliver updates to the system quickly.

With Continuous Integration, the peppermint developers can frequently commit to a shared common repository using CodeBerg. A continuous integration pipeline can automatically run builds, store the artifacts, run unit tests and even conduct code reviews using different tools. We can configure the CI pipeline to be triggered every time there is a commit/merge in the code-base. *(we are not there yet but this is a working goal)*

**What have we done sofar?**

- We re-based on Debian
- We modularized the build process with live-build
- We implemented Calamares
- We have built and are still building a Wiki Documentation repo
- We have built a CD process.
- We have decided on a light install allowing choice.
- We consolidated the Peppermint settings features
- We removed the LXDE parts

**What do we need to do?**

- Build a base on Devuan
- Build net installs
- Rebuild ICE
- Rebuild Update Manager

**Where are we going?**

Peppermint is morphing into its own continuous delivery OS based on the Debian stable repos….with the option to pull from testing as needed. The audience that we want to reach are both new users as well as experienced Linux users.

We want to try to meet that middle ground where it is easy enough to configure after install to fit new user’s needs but also not be “bloated” for the more experienced users, so that it can be configured with, little to no package removal

Further more the option to be non-systemd can help those users who prefer runit init.

This is why it is important for the Peppermint shift to a CI/CD pipeline using DevOps culture, as it allows for innovation the time needed to build features and improvements, as allowed, to our team of developers

Tools used to build Peppermint and custom features
====================================================
**ISO Builder:**

- The way the ISO is created is by using the **live-build** tools provided by Debian. If you are interested in learning about live-build you can click `here <https://live-team.pages.debian.net/live-manual/html/live-manual/index.en.html>`_ to read the live-build manual.

- We have chosen to be very modular so that we can make major or minor changes to the ISO as we build new features or fix issues that the community has found.
- You can view our entire builder code out on codeberg.org by clicking `here <https://codeberg.org/Peppermint_OS/PepDistroConfigs>`_ .




Peppermint Hub
================

**What is the Peppermint Hub?**
The hub is a location that is used to be the launching point for, common XFCE settings as well as other system related utilities. However, this will expand with other features as the project progresses forward. When new tools or custom applications are created they will organized and located somewhere in the Pep Hub accordingly.

The current iteration of the Peppermint Hub is organized like below

.. image:: des.png
   :scale: 60 %
   :alt: Desktop Environment Settings
   
.. image:: hs.png
   :scale: 60 %
   :alt: Desktop Environment Settings
   
**Select Packages**
This application is **NOT** a software store of any kind it is an assistant that will help you install applications that are in the Debian stable repositories, you **DO NOT** need  to use this application to set up your system to your preferences. If you are comfortable in the terminal you can use that method to install applications as needed. 

**Gnome Store, App Image Hub, Flathub, and Snapstore** 
Out of the box these options will open a SSB that will take you to the website of these services. When  you do install flatpak, snapd and gnomestore these options will launch the native associated store you have on the your system


Welcome to Peppermint 
======================

The welcome screen is there to help get you started with your system.

**Install a Web Browser** will open the **Select Packages** window that allows you to pick ant of the web browsers that are in the stable repositories of Debian.

**Peppermint Extras** will test let you choose to install icons, themes and wallpapers for your system. This feature will test to make sure you have a connection to the internet if you do they will wget the extras that are hosted out on the Peppermint Code berg repository `here <https://codeberg.org/Peppermint_OS/>`_ .

**ICE Tutorial and Release Notes** they will open a simple SSB to their perspective websites.

**Open Pephub** will launch the Peppermint Hub application

**Community** these open the in SSBs to their perspective, websites.

hBlock
======
**What is this for?**

hBlock is a POSIX-compliant shell script that gets a list of domains that serve ads, tracking scripts and malware from multiple sources and creates a hosts file, among other formats, that prevents your system from connecting to them.

**warning** hBlock by default replaces the hosts file of your system, consider making a backup first if you have entries you want to preserve.

The default behavior of hBlock can be adjusted with multiple options. Use the --help option 

To enable hBlock you can open a terminal type

.. code:: shell

	hblock
	
To disable hBlock you can open a terminal type
	
.. code:: shell

	hblock -S none -D none



	


   
   


